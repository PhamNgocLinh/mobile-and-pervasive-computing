// Using Model here
// var User 			= require('./models/user');
// var Post 			= require('./models/post');
// var PostsSetiment 	= require('./models/post-sentiment');
// var Comment 		= require('./models/comment');
// var CommentSentiment = require('./models/comment-sentiment');


var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens

// Define Application API
module.exports = function(app) {
	// Get default api
	app.get('/api', function(req, res) {
		res.send("Hello world!");
	});

	// Get all posts
	app.get('/api/posts', function(req, res) {
		Post.find(function(err, posts) {
			if(err) throw err;

			res.json(posts);
		});
	});

	app.get('/api/posts/:page_number.:page_sise', function(req, res) {
		var pn = req.params.page_number;
		var ps = req.parems.page_sise;

		Post.find(function(err, posts) {
			if(err) throw err;

			res.json(posts);
		});
	});

	
	// Get all requests in otherwise
	app.get('*', function(req, res) {
		res.sendfile('public/index.html');
	})
}