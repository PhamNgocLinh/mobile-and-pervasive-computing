// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('History', new Schema({ 
    fid         : String,
    message     : String,
    from_user   : String,
    to_user     : String,
    post_id     : String,
    parent_id   : String,
    likes_count : String,
    replies_count   : String,
    sentiment   : String,
    created_date    : Date,
    last_crawled    : Date,
    updated_at  : Date,
    created_at  : Date,
    is_editing : Boolean
}));