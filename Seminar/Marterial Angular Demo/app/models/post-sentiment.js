// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('posts_sen', new Schema({ 
    fid             : String,
    message         : String,
    sentiment       : String,
    newsentiment    : String,
    kwsentiment     : [],
    author			: String,
    last_update		: Date
}));