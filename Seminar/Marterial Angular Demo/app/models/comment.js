// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Comment', new Schema({ 
    fid             : String,
    message         : String,
    from_user       : String,
    to_user         : String,
    parent_id       : String,
    created_date    : Date,
    last_crawled    : Date,
    updated_at      : Date,
    created_at      : Date,
    sentiment       : String,
    like            : Array,
    count_likes                 : Number,
    count_shares                : Number,
    count_comments              : Number,
    count_negative_comments     : Number,
    count_positive_comments     : Number,
    count_neutral_comments      : Number,
    count_unknown_comments      : Number,
    is_editing      : Boolean
}));