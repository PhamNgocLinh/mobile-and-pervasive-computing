$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();

	   $(".sidebar-menu li .drop-link").on('click', function () {
	      $(this).toggleClass("active");
	   });
	   $(".main-sidebar nav .sidebar-menu.submenu li a").on('click',function () {
	       $(".main-sidebar nav .sidebar-menu.submenu li a.active").removeClass("active");
	      $(this).addClass("active");
	   });

	$(".menu-toggle").on('click', function () {
      $(".sidebar-menu").toggleClass("display-menu");
   });

   if ($(window).width() < 1200) {
      $(".sidebar-menu li a").on('click', function () {
         if($(this).hasClass("drop-link")){

         }else{
            $(".sidebar-menu").removeClass("display-menu");
            $(".main-sidebar li a").removeClass("active");
            $(".main-sidebar nav .sidebar-menu.submenu").removeClass("in");
         }

      });
   };


   $(window).resize(function() {
      if ($(window).width() < 1200) {
         $(".sidebar-menu li a").on('click', function () {
            if($(this).hasClass("drop-link")){

            }else{
               $(".sidebar-menu").removeClass("display-menu");
               $(".main-sidebar li a").removeClass("active");
               $(".main-sidebar nav .sidebar-menu.submenu").removeClass("in");
            }

         });
      }

   });
});