angular.module("PostService", []).factory("PostService", ['$http', function($http) {
	
	var postSer = {
		
		// Get all posts
		getAllPosts : function() {
			return $http.get('/api/posts');
		},

		// Create a post
		createPost : function(post) {
			return $http.post('/api/posts', post);
		},

		// Delete a post
		deletePost : function(post_id) {
			return $http.post('/api/posts' + post_id);
		},

		// Get all posts
		getAllPostsSentiment : function() {
			return $http.get('/api/postssentiment');
		},

		// Create a post
		createPostSentiment : function(postsen) {
			return $http.post('/api/postssentiment', postsen);
		},

		// Delete a post
		deletePostSentiment : function(postsen_id) {
			return $http.post('/api/postssentiment' + postsen_id);
		}

	}

	return postSer;

}]);