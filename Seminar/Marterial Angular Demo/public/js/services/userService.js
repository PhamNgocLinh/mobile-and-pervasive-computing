angular.module("UserService", []).factory("UserService", ['$http', function($http) {
	var userSer = {
		// Get token
		getUserToken : function(user) {
			return $http.post('/api/authenticate', user);
		},

		// Delete token
		deleteUserToken : function() {
			return $http.post('/api/authenticate/signout');
		},


		// Get all users
		getAllUser : function () {
			return $http.get('/api/user');
		},

		// Create a user
		createUser : function(user) {
			return $http.post('/api/user', user);
		},

		// Delete a user
		deleteUser : function(user_id) {
			return $http.post('/api/user' + user_id);
		}
	}

	return userSer;
}]);