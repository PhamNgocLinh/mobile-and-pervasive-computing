angular.module("CommentService", []).factory("CommentService", ['$http', function($http) {
	
	var commentSer = {
		
		// Get all posts
		getAllComments : function() {
			return $http.get('/api/comments');
		},

		// Create a post
		createComment : function(post) {
			return $http.post('/api/comments', post);
		},

		// Delete a post
		deleteComment : function(post_id) {
			return $http.post('/api/comments' + post_id);
		},

		// Get all comments
		getAllCommentsSentiment : function() {
			return $http.get('/api/commentsentiment');
		},

		// Create a post
		createCommentSentiment : function(commentsen) {
			return $http.post('/api/commentsentiment', commentsen);
		},

		// Delete a post
		deleteCommentSentiment : function(commentsen_id) {
			return $http.post('/api/commentsentiment' + commentsen_id);
		}

	}

	return commentSer;

}]);