angular.module("AuthController", ['ngStorage'])
.controller("AuthController", function($scope, $http, UserService, $localStorage, $sessionStorage) {
	$scope.getAllData = function() {
		UserService.getAllUser()
			.success(function(data) {
				console.log(data);
			})
			.error(function(data) {
				alert("Fail");
			});
	}

	$scope.signin = function() {
		UserService.getUserToken($scope.account)
		.success(function(data) {
			if(data.success) {
				console.log("Get token success");
				console.log(data);
				$sessionStorage.token = data.token;
				$sessionStorage.username = data.username;
				window.location.href="/";
			} else {
				console.log("Get not token success");
				console.log(data);
				$sessionStorage.token = null;
				$sessionStorage.username = null;
			}
		})
		.error(function(data) {
			console.log("Get token fail");
			console.log(data);
			$sessionStorage.token = null;
			$sessionStorage.username = null;
		});
	}


	$scope.signup = function() {
		UserService.createUser($scope.userCreate)
		.success(function(data) {
			console.log("Signup checking on client");
			
			if(data.success) {
				console.log("Account create sucess");
				console.log(data);
				window.location.href = "/login";
			} else {
				console.log("Account create fail");
				console.log(data);
			}
		})
		.error(function(data) {
			console.log("Signup fail");
			console.log(data);
		});
	}

})