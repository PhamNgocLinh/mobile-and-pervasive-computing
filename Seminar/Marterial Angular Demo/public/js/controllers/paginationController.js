// // Code goes here
angular.module("PaginationController", []).controller("PaginationController", function($scope) {
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  $scope.meals = [];

  $scope.pageChangeHandler = function(num) {
      console.log('meals page changed to ' + num);
      $scope.pageCurrent = num;
      return num;
  };
});