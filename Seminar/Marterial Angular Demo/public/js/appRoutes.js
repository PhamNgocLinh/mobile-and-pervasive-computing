angular.module('AppRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	
	$routeProvider
		
		// Dashboard
		.when('/', {
			templateUrl 	: 'views/home.html',
			controller 		: 'MainController'
		})

		// Comment page
		.when('/login', {
			templateUrl 	: 'views/login.html',
			controller 		: 'MainController'
		})

		// Comment page
		.when('/register', {
			templateUrl 	: 'views/register.html',
			controller 		: 'MainController'
		})

		.otherwise({
			redirectTo	: '/'
		});

	$locationProvider.html5Mode(true);

}]);